﻿namespace UI.Scripts.MVVM.Interfaces
{
    /// <summary>
    /// Data class view
    /// </summary>
    public interface IResultViewModel
    {
        int Score{ get; }

        bool IsWin { get; }
    }
}
