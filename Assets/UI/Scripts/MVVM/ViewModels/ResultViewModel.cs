﻿using UI.Scripts.MVVM.Interfaces;

namespace UI.Scripts.MVVM.ViewModels
{
    public class ResultViewModel : IResultViewModel
    {
        public int Score
        {
            get
            {
                return score;
            }
        }

        public bool IsWin
        {
            get
            {
                return isWin;
            }
        }

        private int score;

        private bool isWin;

        public ResultViewModel(int score,bool isWin)
        {
            this.score = score;
            this.isWin = isWin;
        }
    }
}
