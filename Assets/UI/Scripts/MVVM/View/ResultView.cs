﻿using UnityEngine;
using UnityEngine.UI;
using UI.Scripts.MVVM.Interfaces;
using UnityEngine.SceneManagement;

namespace UI.Scripts.MVVM.Views
{
    public class ResultView : MonoBehaviour
    {
        private const string FINAL_SCORE_TEXT = "Your score : {0}";

        private const string WIN_TEXT = "You win";

        private const string LOSE_TEXT = "You lose";

        [SerializeField] private Text winOrLose,scoreText;

        public void Bind(IResultViewModel viewModel)
        {
            scoreText.text = string.Format(FINAL_SCORE_TEXT, viewModel.Score);
            winOrLose.text = viewModel.IsWin ? WIN_TEXT : LOSE_TEXT;
        }
    }
}
