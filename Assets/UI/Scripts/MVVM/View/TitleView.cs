﻿using UnityEngine;
using UI.Scripts.MVVM.Interfaces;
using UnityEngine.UI;

namespace UI.Scripts.MVVM.Views
{
    public class TitleView : MonoBehaviour 
    {
        [SerializeField] private Button startBtn;

        private ITitleViewModel viewModel;

        // Use this for initialization
        void Start()
        {
            startBtn.onClick.AddListener(()=> {
                viewModel.OnClickStartButton();
            });
        }

        public void Bind(ITitleViewModel viewModel) => this.viewModel = viewModel;
    }
}
