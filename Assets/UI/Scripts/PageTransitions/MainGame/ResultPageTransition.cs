﻿using System.Collections;
using UI.Scripts.MVVM.ViewModels;
using UI.Scripts.MVVM.Views;

namespace UI.Scripts.PageTransitions.MainGame
{
    public class ResultPageTransition : PageTransition
    {
        public override IEnumerator LoadAsync()
        {
            // Nothing to fetch from back-end so just early return
            yield return null;
        }
        public override void BindLoadedModels()
        {
            var viewModel = new ResultViewModel(GameStatictis.PlayerPoint,GameStatictis.PlayerPoint > GameStatictis.OpponentPoint);
            _pageInstance.GetComponent<ResultView>().Bind(viewModel);
        }
    }
}
