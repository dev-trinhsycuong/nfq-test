﻿using UnityEngine;

public class CardSO : ScriptableObject
{
    [SerializeField]
    private Sprite avatar;

    public Sprite Avatar
    {
        get
        {
            return avatar;
        }
    }

    [SerializeField]
    private string name;

    public string Name
    {
        get
        {
            return name;
        }
    }

    [SerializeField]
    private string description;

    public string Description
    {
        get
        {
            return description;
        }
    }

    [SerializeField]
    private int attackValue;

    public int AttackValue
    {
        get
        {
            return attackValue;
        }
    }

    [SerializeField]
    private int healthValue;

    public int HealthValue
    {
        get
        {
            return healthValue;
        }
    }
}
