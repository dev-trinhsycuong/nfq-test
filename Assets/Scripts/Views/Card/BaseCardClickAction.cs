﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using Scripts.MainGame.Events;
using Scripts.ViewModels.Cards;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views.Cards
{
    public enum CardClickAction
    {
        NONE,
        PUT_TO_PLAYER_DECK,
        PUT_TO_BATTLE_DECK
    }

    public class BaseCardClickAction
    {
        public virtual void Do(params object[] args) { }
    }
}
