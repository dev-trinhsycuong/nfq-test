﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using Scripts.MainGame.Events;
using Scripts.ViewModels.Cards;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views.Cards
{
    public class CardView : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        [SerializeField] private Button button;

        [SerializeField] private Image avatar;

        [SerializeField] private Text nameCard,description,attack,health;

        private CardViewModel viewModel;

        public CardViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
        }

        private int cardIndex;

        private const string FAN_ANIMATION_NAME = "Fan_Card_To_{0}";

        private const string PUT_CARD_PLAYERDECK_ANIMATION_NAME = "PutToPlayerDeck_{0}_To_{1}";

        private const string PUT_CARD_OPPONENTDECK_ANIMATION_NAME = "PutToEnemyDeck_{0}_To_{1}";

        private const string PLAYER_PUT_CARD_BATTLEDECK_ANIMATION_NAME = "PutToBattleDeck_{0}_To_{1}";

        private const string OPPONENT_PUT_CARD_BATTLEDECK_ANIMATION_NAME = "Opponent_PutToBattleDeck_{0}_To_{1}";

        private BaseCardClickAction clickAction = null;

        private Dictionary<CardClickAction, BaseCardClickAction> clickActionMapping = new Dictionary<CardClickAction, BaseCardClickAction>()
        {
            {CardClickAction.NONE,null },
            {CardClickAction.PUT_TO_PLAYER_DECK,new PutToPlayerDeckCardClickAction() },
            {CardClickAction.PUT_TO_BATTLE_DECK,new PutToBattleDeckCardClickAction() }
        };

        public void Bind(CardViewModel viewModel,int cardIndex)
        {
            avatar.sprite = viewModel.Avatar;
            nameCard.text = viewModel.Name;
            description.text = viewModel.Description;
            attack.text = viewModel.AttackValue.ToString();
            health.text = viewModel.HealthValue.ToString();

            this.viewModel = viewModel;
            this.cardIndex = cardIndex;
        }

        public void FanAnimationToIndex(int index)
        {
            animator.enabled = true;
            animator.Play(string.Format(FAN_ANIMATION_NAME,index));
        }

        public void PutCardInPlayerDeck(int from,int to)
        {
            animator.enabled = true;
            animator.Play(string.Format(PUT_CARD_PLAYERDECK_ANIMATION_NAME, from,to));
        }

        public void PutCardInOpponentDeck()
        {
            PutCardInOpponentDeck(this.cardIndex, DeckController.EmptyOpponentDeckIndex);
            DeckController.PutCardOpponentToDeckFrom(this, this.cardIndex);
        }

        public void PutCardInOpponentDeck(int from, int to)
        {
            animator.enabled = true;
            animator.Play(string.Format(PUT_CARD_OPPONENTDECK_ANIMATION_NAME, from, to));
        }

        public void PlayerPutCardInBattleDeck(int from, int to)
        {
            animator.enabled = true;
            animator.Play(string.Format(PLAYER_PUT_CARD_BATTLEDECK_ANIMATION_NAME, from, to));
        }

        public void OpponentPutCardInBattleDeck(int from, int to)
        {
            animator.enabled = true;
            animator.Play(string.Format(OPPONENT_PUT_CARD_BATTLEDECK_ANIMATION_NAME, from, to));
        }

        public void SetClickAction(CardClickAction action)
        {
            clickAction = clickActionMapping[action];
        }

        // Start is called before the first frame update
        void Start()
        {
            button.onClick.AddListener(() => {
                if (clickAction != null)
                {
                    clickAction.Do(this, this.cardIndex);
                    clickAction = null;
                }
            });
        }
    }
}
