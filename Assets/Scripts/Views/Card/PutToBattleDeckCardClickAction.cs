﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using Scripts.MainGame.Events;
using Scripts.ViewModels.Cards;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views.Cards
{
    public class PutToBattleDeckCardClickAction : BaseCardClickAction
    {
        public override void Do(params object[] args)
        { 
            if(args[0] is MonoBehaviour)
            {
                var view = args[0] as CardView;
                view.StartCoroutine(PlayerPutInCard(view));
            }
        }

        IEnumerator PlayerPutInCard(CardView view)
        {
            DeckController.PlayerPutInCard(view);

            foreach (var card in DeckController.AllCard)
            {
                card.SetClickAction(Views.Cards.CardClickAction.NONE);
            }

            yield return new WaitForSeconds(1.5f);

            EventCenter.OnFighting();
        }
    }
}
