﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using Scripts.MainGame.Events;
using Scripts.ViewModels.Cards;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views.Cards
{
    public class PutToPlayerDeckCardClickAction : BaseCardClickAction
    {
        public override void Do(params object[] args) 
        {
            var view = args[0] as CardView;
            var index = (int)args[1];

            view.PutCardInPlayerDeck(index, DeckController.EmptyPlayeyDeckIndex);
            DeckController.PutCardPlayerToDeckFrom(view, index);

            if (DeckController.EmptyPlayeyDeckIndex == -1)
            {
                EventCenter.OnFinishedChooseCard();
            }
        }
    }
}
