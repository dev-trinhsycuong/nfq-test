﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Views
{
    public class MainGameView : MonoBehaviour
    {
        [SerializeField] private Text playerPoint,opponentPoint;

        private const string PLAYER_POINT_FORMAT = "Player : {0}";

        private const string OPPONENT_POINT_FORMAT = "Opponent : {0}";

        // Start is called before the first frame update
        void Start()
        {
            GameStatictis.OnPlayerPointChanged = () =>
            {
                playerPoint.text = string.Format(PLAYER_POINT_FORMAT, GameStatictis.PlayerPoint.ToString());
            };

            GameStatictis.OnOpponentPointChanged = () =>
            {
                opponentPoint.text = string.Format(OPPONENT_POINT_FORMAT, GameStatictis.PlayerPoint.ToString());
            };
        }
    }
}
