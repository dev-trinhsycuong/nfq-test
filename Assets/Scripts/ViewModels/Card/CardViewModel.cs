﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.ViewModels.Cards
{
    public class CardViewModel
    {
        [SerializeField]
        private Sprite avatar;

        public Sprite Avatar
        {
            get
            {
                return avatar;
            }
        }

        [SerializeField]
        private string name;

        public string Name
        {
            get
            {
                return name;
            }
        }

        [SerializeField]
        private string description;

        public string Description
        {
            get
            {
                return description;
            }
        }

        [SerializeField]
        private int attackValue;

        public int AttackValue
        {
            get
            {
                return attackValue;
            }
        }

        [SerializeField]
        private int healthValue;

        public int HealthValue
        {
            get
            {
                return healthValue;
            }
        }

        public bool IsPlayer = false;

        public CardViewModel(CardSO model)
        {
            name = model.Name;
            avatar = model.Avatar;
            description = model.Description;
            attackValue = model.AttackValue;
            healthValue = model.HealthValue;
        }
    }
}
