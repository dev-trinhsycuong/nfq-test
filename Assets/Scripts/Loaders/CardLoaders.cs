﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Views.Cards;
using UnityEngine;

namespace Scripts.Loaders
{
    public class CardLoaders
    {
        private const string SCRIPTABLE_CARD_PATH = "ScriptableCards";

        private const string PREFAB_CARD_PATH = "Prefabs/Card/Card";

        private const string ROOT_CANVAS = "MainGameCanvas";

        public static CardSO[] LoadAll()
        {
            return Resources.LoadAll<CardSO>(SCRIPTABLE_CARD_PATH);
        }

        public static CardView LoadCardView()
        {
            var view = Resources.Load<CardView>(PREFAB_CARD_PATH);
            return GameObject.Instantiate(view, GameObject.Find(ROOT_CANVAS).transform);
        }
    }
}
