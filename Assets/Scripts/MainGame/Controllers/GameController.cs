﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Loaders;
using Scripts.MainGame.Events;
using Scripts.MainGame.States;
using Scripts.ViewModels.Cards;
using UnityEngine;

namespace Scripts.MainGame.Controllers
{
    public class GameController : MonoBehaviour
    {
        private MainGameState mainGameState;

        public MainGameState MainGameState
        {
            get { return mainGameState; }
            set {   
                mainGameState = value;
                OnMainGameStateChanged();

            }
        }

        private Dictionary<MainGameState, BaseGameStateAction> stateActionMapping = new Dictionary<MainGameState, BaseGameStateAction>()
        {
            {MainGameState.LOAD_CARDS,new LoadAvatarGameStateAction() },
            {MainGameState.PLAYER_CHOOSE_CARD,new ChooseCardGameStateAction() },
            {MainGameState.IN_DECK_OPPONENT_CARD,new InDeckOpponentCardGameStateAction() },
            {MainGameState.IN_BATTLE,new InBattleGameStateAction() },
            {MainGameState.END,new EndGameStateAction() }
        };

        // Start is called before the first frame update
        void Start()
        {
            MainGameState = MainGameState.LOAD_CARDS;

            EventCenter.OnFinishedChooseCard = () =>
            {
                MainGameState = MainGameState.IN_DECK_OPPONENT_CARD;
            };

            EventCenter.OnInDeckOpponentCard = () =>
            {
                DeckController.UnDeckCard.ForEach(x =>
                {
                    x.PutCardInOpponentDeck();
                });

                MainGameState = MainGameState.IN_BATTLE;
            };

            EventCenter.OnEnd = () =>
            {
                MainGameState = MainGameState.END;
            };
        }

        void OnMainGameStateChanged()
        {
            object args1 = this;
            System.Action onCompletedAction = null;

            switch (mainGameState)
            {
                case MainGameState.LOAD_CARDS:
                    {
                        onCompletedAction = () => { MainGameState = MainGameState.PLAYER_CHOOSE_CARD; };
                        break;
                    }
            }

            stateActionMapping[mainGameState].Do(onCompletedAction,args1);
        }
    }
}
