﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Views.Cards;
using UnityEngine;
using System.Linq;

namespace Scripts.MainGame.Controllers
{
    public class RandomCardData
    {
        public int Index;
        public CardView View;

        public RandomCardData(int index,CardView view)
        {
            this.Index = index;
            this.View = view;
        }
    }

    public static class DeckController
    {
        private static List<CardView> allCard = new List<CardView>();

        public static List<CardView> AllCard
        {
            get
            {
                return allCard;
            }
        }

        public static List<CardView> UnDeckCard
        {
            get
            {
                return allCard.Where(x => !PlayerCard.Any(y => y.Value == x)).ToList();
            }
        }

        private static Dictionary<int, CardView> playerCard = new Dictionary<int, CardView>()
        {
            {0,null},
            {1,null},
            {2,null},
            {3,null},
            {4,null}
        };

        public static Dictionary<int, CardView> PlayerCard
        {
            get
            {
                return playerCard;
            }
        }

        private static Dictionary<int, CardView> enemyCard = new Dictionary<int, CardView>()
        {
            {0,null},
            {1,null},
            {2,null},
            {3,null},
            {4,null}
        };

        public static Dictionary<int, CardView> EnemyCard
        {
            get
            {
                return enemyCard;
            }
        }

        private static Dictionary<int, CardView> InBattleCard = new Dictionary<int, CardView>()
        {
            {0,null},
            {1,null}
        };

        public static int EmptyPlayeyDeckIndex
        {
            get
            {
                for(int i = 0; i < PlayerCard.Count;i++)
                {
                    if (PlayerCard[i] == null) return i;
                }

                return -1;
            }
        }

        public static int EmptyOpponentDeckIndex
        {
            get
            {
                for (int i = 0; i < EnemyCard.Count; i++)
                {
                    if (EnemyCard[i] == null) return i;
                }

                return -1;
            }
        }

        public static int EmptyInBattleDeckIndex
        {
            get
            {
                for (int i = 0; i < InBattleCard.Count; i++)
                {
                    if (InBattleCard[i] == null) return i;
                }

                return -1;
            }
        }

        private static CardView attacker = null;

        public static CardView Attacker
        {
            get
            {
                return attacker;
            }
        }

        private static CardView defender = null;

        public static CardView Defender
        {
            get
            {
                return defender;
            }
        }

        public static bool IsInFighting
        {
            get
            {
                return InBattleCard[0] != null && InBattleCard[1] != null;
            }
        }

        public static int GetCardViewIndexInPlayerDeck(CardView view)
        {
            for (int i = 0; i < PlayerCard.Count; i++)
            {
                if (PlayerCard[i] == view) return i;
            }

            return -1;
        }

        public static RandomCardData RandomOpponentCard
        {
            get
            {
                var availableIndex = EnemyCard.Where(x => x.Value != null).Select(x => x.Key).ToList();

                if (availableIndex.Count <= 0) return null;

                var randomIndex = availableIndex[UnityEngine.Random.Range(0, availableIndex.Count)];

                var card = EnemyCard[randomIndex];
                var randomCardData = new RandomCardData(randomIndex,card);
                return randomCardData;
            }
        }

        public static void PutCardPlayerToDeckFrom(CardView view, int index)
        {
            PlayerCard[EmptyPlayeyDeckIndex] = view;
        }

        public static void PutCardOpponentToDeckFrom(CardView view, int index)
        {
            EnemyCard[EmptyOpponentDeckIndex] = view;
        }

        public static void OpponentPutInCard()
        {
            var randomCardData = RandomOpponentCard;

            if (randomCardData == null) return;

            randomCardData.View.OpponentPutCardInBattleDeck(randomCardData.Index,EmptyInBattleDeckIndex);

            EnemyCard[randomCardData.Index] = null;
            InBattleCard[EmptyInBattleDeckIndex] = randomCardData.View;

            defender = attacker;
            attacker = randomCardData.View;
            attacker.ViewModel.IsPlayer = false;
        }

        public static void PlayerPutInCard(CardView view)
        {
            var index = GetCardViewIndexInPlayerDeck(view);

            view.PlayerPutCardInBattleDeck(index, EmptyInBattleDeckIndex);

            PlayerCard[index] = null;
            InBattleCard[EmptyInBattleDeckIndex] = view;

            defender = attacker;
            attacker = view;
            attacker.ViewModel.IsPlayer = true;
        }

        public static void DestroyBattleCard(CardView view)
        {
            view.gameObject.SetActive(false);
            if (InBattleCard[0] == view) InBattleCard[0] = null;
            else InBattleCard[1] = null;
        }

        public static void StoreCard(CardView view)
        {
            allCard.Add(view);
        }
    }
}
