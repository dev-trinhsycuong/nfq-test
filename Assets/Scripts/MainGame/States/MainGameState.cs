﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MainGame.States
{
    public enum MainGameState
    {
        LOAD_CARDS,
        PLAYER_CHOOSE_CARD,
        IN_DECK_OPPONENT_CARD,
        IN_BATTLE,
        END
    }
}
