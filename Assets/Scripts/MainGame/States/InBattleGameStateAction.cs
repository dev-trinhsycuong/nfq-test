﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using Scripts.MainGame.Events;
using Scripts.MainGame.States.BattleStates;
using UnityEngine;

namespace Scripts.MainGame.States
{
    public class InBattleGameStateAction : BaseGameStateAction
    {
        private BattleGameState battleGameState;

        private MonoBehaviour rootMono;

        public BattleGameState BattleGameState
        {
            get { return battleGameState; }
            set
            {
                battleGameState = value;
                OnBattleGameStateChanged();

            }
        }

        private Dictionary<BattleGameState, BaseBattleGameStateAction> stateActionMapping = new Dictionary<BattleGameState, BaseBattleGameStateAction>()
        {
            {BattleGameState.FIGHTING,new FightingBattleGameStateAction() },
            {BattleGameState.OPPONENT_PUT_IN_CARD,new OpponentPutInCardBattleGameStateAction() },
            {BattleGameState.PLAYER_PUT_IN_CARD,new PlayerPutInCardBattleGameStateAction() }
        };

        public override void Do(System.Action OnCompletedAction, params object[] args)
        {
            if(args[0] is MonoBehaviour)
                rootMono = args[0] as MonoBehaviour;
            BattleGameState = BattleGameState.OPPONENT_PUT_IN_CARD;

            foreach (var card in DeckController.AllCard)
            {
                card.SetClickAction(Views.Cards.CardClickAction.NONE);
            }

            EventCenter.OnFighting = () => { BattleGameState = BattleGameState.FIGHTING; };

            EventCenter.OnPlayerPutInCard = () => { BattleGameState = BattleGameState.PLAYER_PUT_IN_CARD; };

            EventCenter.OnOpponentPutInCard = () => { BattleGameState = BattleGameState.OPPONENT_PUT_IN_CARD; };
        }

        void OnBattleGameStateChanged()
        {
            System.Action onCompletedAction = null;

            switch (battleGameState)
            {
                case BattleGameState.OPPONENT_PUT_IN_CARD:
                    {
                        onCompletedAction = () => { BattleGameState = BattleGameState.PLAYER_PUT_IN_CARD; };
                        break;
                    }
            }

            stateActionMapping[battleGameState].Do(onCompletedAction,rootMono);
        }
    }
}
