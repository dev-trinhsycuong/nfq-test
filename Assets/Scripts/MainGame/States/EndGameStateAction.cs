﻿using System.Collections;
using System.Collections.Generic;
using UI.Scripts.PageTransitions.MainGame;
using UI.Scripts.Route;
using UnityEngine;

namespace Scripts.MainGame.States
{
    public class EndGameStateAction : BaseGameStateAction
    {
        public override void Do(System.Action OnCompletedAction,params object[] args)
        {
            PageRouter.Instance.DoTransition<ResultPageTransition>();
        }
    }
}
