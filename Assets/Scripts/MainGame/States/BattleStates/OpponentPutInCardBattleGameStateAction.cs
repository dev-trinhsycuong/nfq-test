﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using Scripts.MainGame.Events;
using UnityEngine;

namespace Scripts.MainGame.States.BattleStates
{
    public class OpponentPutInCardBattleGameStateAction : BaseBattleGameStateAction
    {
        public override void Do(System.Action OnCompletedAction, params object[] args)
        {
            if (args[0] is MonoBehaviour)
            {
                (args[0] as MonoBehaviour).StartCoroutine(OpponentPutInCard());
            }
        }

        IEnumerator OpponentPutInCard()
        {
            yield return new WaitForSeconds(2);

            DeckController.OpponentPutInCard();

            yield return new WaitForSeconds(1.5f);

            if (DeckController.IsInFighting)
            {
                EventCenter.OnFighting();
            }
            else
            {
                EventCenter.OnPlayerPutInCard();
            }
        }
    }
}
