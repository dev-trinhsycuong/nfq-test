﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using UnityEngine;

namespace Scripts.MainGame.States.BattleStates
{
    public class PlayerPutInCardBattleGameStateAction : BaseBattleGameStateAction
    {
        public override void Do(System.Action OnCompletedAction, params object[] args) 
        {
            Debug.Log("PlayerPutInCardBattleGameStateAction");
            foreach (var card in DeckController.PlayerCard)
            {
                if (card.Value != null)
                    card.Value.SetClickAction(Views.Cards.CardClickAction.PUT_TO_BATTLE_DECK);
            }
        }
    }
}
