﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MainGame.States.BattleStates
{
    public class BaseBattleGameStateAction
    {
        public virtual void Do(System.Action OnCompletedAction, params object[] args) { }
    }
}
