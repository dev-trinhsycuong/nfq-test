﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Controllers;
using Scripts.MainGame.Events;
using UnityEngine;
using System.Linq;

namespace Scripts.MainGame.States.BattleStates
{
    public class FightingBattleGameStateAction : BaseBattleGameStateAction
    {
        public override void Do(System.Action OnCompletedAction, params object[] args) 
        {
            if(DeckController.Attacker.ViewModel.AttackValue >= DeckController.Defender.ViewModel.HealthValue)
            {
                DeckController.DestroyBattleCard(DeckController.Defender);

                if (DeckController.Defender.ViewModel.IsPlayer)
                {
                    EventCenter.OnPlayerPutInCard();
                    GameStatictis.OpponentPoint += 1;

                    if (DeckController.PlayerCard.All(x => x.Value == null))
                    {
                        EventCenter.OnEnd();
                    }
                }
                else
                {
                    EventCenter.OnOpponentPutInCard();
                    GameStatictis.PlayerPoint += 1;

                    if (DeckController.EnemyCard.All(x => x.Value == null))
                    {
                        EventCenter.OnEnd();
                    }
                }
            }
        }
    }
}
