﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MainGame.States.BattleStates
{
    public enum BattleGameState
    {
        OPPONENT_PUT_IN_CARD,
        PLAYER_PUT_IN_CARD,
        FIGHTING
    }
}
