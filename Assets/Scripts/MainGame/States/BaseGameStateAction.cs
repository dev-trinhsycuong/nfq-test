﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MainGame.States
{
    public class BaseGameStateAction
    {
        public virtual void Do(System.Action OnCompletedAction, params object[] args) { }
    }
}
