﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Loaders;
using Scripts.MainGame.Controllers;
using Scripts.ViewModels.Cards;
using UnityEngine;

namespace Scripts.MainGame.States
{
    public class LoadAvatarGameStateAction : BaseGameStateAction
    {
        public override void Do(System.Action OnCompletedAction,params object[] args)
        {
            if(args[0] is MonoBehaviour)
            {
                (args[0] as MonoBehaviour).StartCoroutine(LoadCards());
            }
        }

        IEnumerator LoadCards()
        {
            var cards = CardLoaders.LoadAll();
            for (int i = 0; i < cards.Length; i++)
            {
                var cardView = CardLoaders.LoadCardView();
                var viewModel = new CardViewModel(cards[i]);
                cardView.Bind(viewModel,i);

                cardView.FanAnimationToIndex(i);

                DeckController.StoreCard(cardView);

                yield return new WaitForSeconds(0.25f);
            }

            yield return new WaitForSeconds(2);

            foreach(var card in DeckController.AllCard)
            {
                card.SetClickAction(Views.Cards.CardClickAction.PUT_TO_PLAYER_DECK);
            }
        }
    }
}
