﻿using System.Collections;
using System.Collections.Generic;
using Scripts.MainGame.Events;
using UnityEngine;

namespace Scripts.MainGame.States
{
    public class InDeckOpponentCardGameStateAction : BaseGameStateAction
    {
        public override void Do(System.Action OnCompletedAction,params object[] args)
        {
            EventCenter.OnInDeckOpponentCard();
        }
    }
}
