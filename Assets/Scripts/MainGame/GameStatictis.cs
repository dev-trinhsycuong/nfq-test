﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameStatictis
{
    private static int playerPoint = 0;

    public static int PlayerPoint
    {
        get
        {
            return playerPoint;
        }
        set
        {
            playerPoint = value;
            OnPlayerPointChanged();
        }
    }

    private static int opponentPoint = 0;

    public static int OpponentPoint
    {
        get
        {
            return opponentPoint;
        }
        set
        {
            opponentPoint = value;
            OnOpponentPointChanged();
        }
    }

    public static System.Action OnPlayerPointChanged;

    public static System.Action OnOpponentPointChanged;
}
