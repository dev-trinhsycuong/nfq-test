﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.MainGame.Events
{
    public static class EventCenter
    {
        public static System.Action OnFinishedChooseCard;

        public static System.Action OnInDeckOpponentCard;

        public static System.Action OnOpponentPutInCard;

        public static System.Action OnPlayerPutInCard;

        public static System.Action OnFighting;

        public static System.Action OnEnd;
    }
}
